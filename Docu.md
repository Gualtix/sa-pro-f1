# Elecciones Mediante Blockchain
### Walter Morales - 200915518

[Presentacion](https://www.canva.com/design/DAFDOGxhCVY/cwzt5dQg6HPhIyc85gmaJA/view?utm_content=DAFDOGxhCVY&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton)

![](img/Arq.png)

## Microservicios Identificados

- Crear Usuario
- Escrutinio
- Registrar Voto
- Login
- Renap
- Validar Voto
- Interconexion
- DB Access
- Blockchain
- Auditoria


## Comunicación entre los servicios

Se prevee que la comunicacion sea controlada por un Enterprise Service Bus (ESB) que para este caso
especifico se implementara como un Middaleware utilizando un protocolo de comunicacion de mensajes
de tipo JSON. Especificamente mediante una RESTful API. 

Esta api centralizara las peticiones y servira de intefaz entre los servicios y tambien
sera el punto de convergencia para la validacion de cadena de bloques y la comunicacion con
otros sistemas.

![](img/2022-06-09-00-50-02.png)

## Diagrama de actividades del microservicio de autenticación

![](img/actiauth.png)

## Diagrama de actividades de emisión de voto

![](img/activoto.png)

## Diagrama de actividades del sistema de registro (enrollamiento) de ciudadanos

![](img/arctiempa.png)

## Descripción de la seguridad de la aplicación

Se planea implementar un patron de seguridad basado en diferentes factores que permitan 
la autenticacion de los usuarios y eviten el fraude de datos. Esto mediante los siguientes
puntos:

- Autenticacion de usuarios por 2FA
- Autenticacion de usuarios por Tocken generado al momento de empadronarse y registrar el voto
- Registro de Localidad y Direccion Mac del Dispositivo
- Validacion de los resultados mediante una cadena de bloques (Blockchain)
- Protocolo de comunicacion mediante HTTPS

![](img/2fa.png)

## Contratos de Conexion

Segun se solicito, acontinuacion se muestran los links correspodientes a los contratos
trabajados en el estandar: Openapi y Swagger.

[microServicio Autenticacion](https://app.swaggerhub.com/apis-docs/Gualtix/msAutenticacion/1.0.0)
[microServicio dbAccess](https://app.swaggerhub.com/apis-docs/Gualtix/msDBAccess/1.0.0)
[microServicio Escrutinio](https://app.swaggerhub.com/apis-docs/Gualtix/msEscrutinio/1.0.0)

Los respectivos archivos YAML estan disponibles en la carpeta: contracts



## Descripción del uso de la tecnología blockchain

Se planea utilzar Python como lenguaje de programacion para la implementacion de la blockchain
mediante el uso de la libreria: hashlib. Esta libreria permite realizar operaciones de hash
encriptacion y desencriptacion. Ademas de esto permite realizar operaciones de firma digital
y verificacion de firma digital. Acontinuacion se muestran dos fuentes de implementacion de 
la blockchain:

[Python Micro Blockchain](https://morioh.com/p/b67da26e40ec)
[Python Blockchain Genesis](https://geekflare.com/es/create-a-blockchain-with-python/)

La logica de esta implementacion esta basada en la comunicacion P2P entre EBS para lo cual es sumamente
importante que los nodos se conecten entre si mediante un protocolo de comunicacion de mensajes de tipo
JSON. Asi mismo que las interfaces de comunicacion sean RESTful y los contratos de transacciones sean
estandarizados. La logica es la siguiente:

![](img/block.png)

## Documentación de las CI Pipelines para los servicios

Se han planificado las siguientes pipelines para los servicios:

    - Pipeline de creacion de usuario
    - Pipeline de escrutinio
    - Pipeline de registro de voto
    - Pipeline de login
    - Pipeline de renap
    - Pipeline de DB Access
    - Pipeline de Blockchain
    - Pipeline de Auditoria
    - Pipeline de Autenticacion
    - Pipeline de Frontend

Cada Pipeline de dividira en los siguientes stages:

    - build
    - test
    - deploy

![](img/2022-06-09-22-43-26.png)

Se utilizara Gitlab CI para la gestion de las pipelines. Manejando la ejecucion de cada stage 
mediante los gitlab runners.

### Archivo de Configuracion de Gitlab CI .gitlab-ci.yml tomando mslogin como ejemplo

```yaml
image: node:latest

stages:
  - build
  - test
  - deploy

build-mslogin:
  stage: build
  only:
    - "develop"
  script:
    - cd backend/mslogin
    - npm install
  artifacts:
    expire_in: 1h
    paths:
      - backend/mslogin/node_modules/
  cache:
    paths:
      - backend/mslogin/node_modules/
  tags:
    - SAPRO2022

test-mslogin:
  stage: test
  only:
    - "develop"
  dependencies:
    - build-mslogin
  script:
    - cd backend/mslogin
    - npm test
  tags:
    - SAPRO2022

#construccion de imagenes
build_CI:
  stage: build
  image: docker:latest
  only:
    - "main"
  variables:
    DOCKER_REGISTRY: "docker.io"
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - docker login -u $DOCKERHUB_USER -p $DOCKERHUB_KEY             
    - docker build -t "20915518/mslogin:latest"  ./backend/mslogin/ 
    - docker push "200915518/mslogin:latest"                         

  after_script:
    - docker logout
  tags:
    - SAPRO2022

kubernetes-deploy:
  stage: deploy
  only:
    - "main"
  script:
    - docker rmi $(docker images -q)
    - docker pull 200915518/mslogin:latest
    - rm -rf repo
    - mkdir -p repo
    - cd repo
    - git clone https://personal:iFgmC1TKrFy2PSGnLB4a@gitlab.com/Gualtix/sa-pro-f1.git
    - cd "SAPRO2022"
    - kubectl expose deployment backend --type=LoadBalancer --name=backend-service --load-balancer-ip=34.139.17.223
    - kubectl rollout restart deploy
    - kubectl get svc
    - kubectl get pods
  tags:
    - "SAPRO2022"

``` 






